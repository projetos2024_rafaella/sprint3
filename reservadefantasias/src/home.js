import React from "react";
import { View, TouchableOpacity, Text, Image, StyleSheet } from "react-native";
import { useNavigation } from "@react-navigation/native";
import api from "./axios/axios";
import DateTimePicket from "./components/datePicker";
import CheckDays from "./components/checkDays";

const Home = () => {
  const navigation = useNavigation();

  const handleLogin = () => {
    navigation.navigate("Login");
  };

  const handleCadastro = () => {
    navigation.navigate("Cadastro");
  };

  const handleEscolha = () => {
    navigation.navigate("Escolha");
  }
  

  return (
    <View style={styles.container}>
      <Image
        source={require("../assets/fantasia_hd.png")}
        style={styles.image}
      />

      <View style={styles.content}>
        <Text style={styles.title}>Reserve sua</Text>
        <Text style={styles.title1}>Fantasia ou</Text>
        <Text style={styles.title2}>Seus acessórios</Text>
        <Text style={styles.title3}>Aqui </Text>
        <View style={{ top: 80 }}>
          <TouchableOpacity
            style={styles.button}
            onPress={() => navigation.navigate("Login")}
          >
            <Text style={styles.buttonText}>Entrar</Text>
          </TouchableOpacity>

          <TouchableOpacity onPress={() => navigation.navigate("Cadastro")}>
            <Text style={styles.ainda}>
              Ainda não tem uma conta?
            </Text>
            <View style={styles.buttonContainer}></View>
            <Button
                title="Reservar"
                onPress={() => handleReservation(item.number)}
                color="blue"
              />
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#7435F0",
  },
  image: {
    height: 300,
    width: 400,
    borderRadius: 20,
    marginBottom: 20,
    left: 25,
    top: 90
  },
  content: {
    paddingHorizontal: 22,
  },
  title: {
    fontSize: 50,
    fontWeight: "800",
    color: "white",
    marginBottom: 30,
    textAlign: "left",
    top: 70,
  },
  title1: {
    fontSize: 50,
    fontWeight: "800",
    color: "white",
    marginBottom: 30,
    textAlign: "left",
    top: 30,
  },
  title2: {
    fontSize: 50,
    fontWeight: "800",
    color: "white",
    marginBottom: 30,
    textAlign: "left",
    top: -10,
  },

  title3: {
    fontSize: 50,
    fontWeight: "800",
    color: "white",
    marginBottom: 30,
    textAlign: "left",
    top: -50,
  },
  button: {
    backgroundColor: "#F09C35",
    paddingVertical: 15,
    paddingHorizontal: 30,
    borderRadius: 5,
    marginBottom: 20,
  },
  buttonText: {
    color: "white",
    fontSize: 20,
    textAlign: "center",
    fontWeight:"bold"
  },
  ainda: {
    fontSize: 16,
    color: "white",
    textAlign:"center",
    textDecorationLine: 'underline'
  },
});

export default Home;

