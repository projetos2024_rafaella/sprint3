import React, { useState } from "react"; // Importa o React e a função useState do pacote 'react'
import { View, TouchableOpacity, StyleSheet, FlatList, ScrollView, Image, Text, Dimensions } from "react-native"; // Importa componentes essenciais do React Native para construir a interface
import { useNavigation } from "@react-navigation/native"; // Importa o hook useNavigation do pacote '@react-navigation/native' para acessar a navegação
import { TextInput } from "react-native-gesture-handler"; // Importa o componente TextInput do pacote 'react-native-gesture-handler' para entrada de texto

const windowWidth = Dimensions.get("window").width; // Obtém a largura da janela do dispositivo

function Fantasias() { // Define o componente de função Fantasias
    const data = [ // Define uma array de objetos com dados das fantasias e acessórios
        { id: 1, name: "Chapeleiro maluco feminino", preço: "R$160,00", image: require('../assets/cartola.png') },
        { id: 2, name: "Stitch", preço: "R$150,00", image: require('../assets/stich.png') },
        { id: 3, name: "Palhaço", preço: "R$100,00", image: require('../assets/palhacos.png') },
        { id: 4, name: "Joaninha", preço: "R$110,00", image: require('../assets/joaninha.png') },
        { id: 1, name: "Acessórios piratas", preço: "R$65,00", image: require('../assets/acessorioPirata.png') },
        { id: 2, name: "Acessórios minnie", preço: "R$70,00", image: require('../assets/minnie.png') },
        { id: 3, name: "Acessórios gato", preço: "R$50,00", image: require('../assets/gato.png') },
        { id: 4, name: "Acessórios sapo", preço: "R$50,00", image: require('../assets/sapo.png') },
    ];

    const navigation = useNavigation(); // Inicializa a navegação usando o hook useNavigation

    const usersPress = (fantasias) => { // Define uma função de retorno de chamada que será chamada quando um item for pressionado
        navigation.navigate("Detalhes", { fantasias }); // Navega para a tela de detalhes passando os dados da fantasia selecionada
    };

    return ( // Retorna a estrutura da interface do componente
        <View style={styles.container}> 
                <Text style={styles.headerText}>Fantasias e acessórios aqui</Text> 
            <ScrollView style={{ flex: 1 }}>
                <FlatList // Define uma FlatList para renderizar a lista de fantasias e acessórios
                    style={{}} // Estilo da FlatList
                    data={data} // Dados a serem renderizados na lista
                    keyExtractor={(item) => item.id.toString()} // Função para extrair chaves únicas para cada item da lista
                    numColumns={2} // Número de colunas na lista
                    scrollEnabled={false} // Desabilita a rolagem da FlatList
                    renderItem={({ item }) => ( // Função para renderizar cada item da lista
                        <TouchableOpacity style={styles.buttonProductColumn} onPress={() => usersPress(item)}> 
                            <Image source={item.image} style={styles.imageColumn} /> 
                            <View style={styles.detalhesContainer}> 
                                <Text style={styles.nameColumn}>{item.name}</Text>
                                <Text style={styles.precoColumn}>{item.preço}</Text>
                            </View>
                        </TouchableOpacity>
                    )}
                />
            </ScrollView>
        </View>
    );

}

const styles = StyleSheet.create({ // Define os estilos usando StyleSheet
    container: { // Estilos para o contêiner principal
        flex: 1, // Ocupa todo o espaço disponível
    },
    buttonProductColumn: { // Estilos para cada item da lista
        width: windowWidth / 2 - 20, // Largura dos itens da lista
        
        height: 110, // Altura dos itens da lista
        borderRadius: 8, // Raio da borda dos itens da lista
        borderColor: "black", // Cor da borda dos itens da lista
        marginBottom: 75, // Margem inferior dos itens da lista
        top:85,
        left:15,
        marginHorizontal: 3, // Margem horizontal dos itens da lista
        flexDirection: 'row', // Layout em linha
        alignItems: 'center', // Alinhamento dos itens no eixo vertical
        justifyContent: 'center', // Alinhamento dos itens no eixo horizontal
    },
    
    imageColumn: { // Estilos para as imagens dos itens da lista
        width: "50%", // Largura da imagem
        height: "110%", // Altura da imagem
        resizeMode: 'contain', // Redimensionamento da imagem
        
    },
    detalhesContainer: { // Estilos para os detalhes dos itens da lista
        flex: 1, // Ocupa todo o espaço disponível
        marginLeft: 10, // Margem à esquerda
    },
    nameColumn: { // Estilos para o nome dos itens da lista
        fontSize: 16, // Tamanho da fonte
        fontWeight: 'bold', // Estilo da fonte (negrito)
    },
    precoColumn: { // Estilos para o preço dos itens da lista
        fontSize: 14, // Tamanho da fonte
    },
    header: { // Estilos para o cabeçalho da tela
        justifyContent: 'center', // Alinhamento no eixo horizontal
        alignItems: 'center', // Alinhamento no eixo vertical
        marginTop: -50, // Margem superior negativa
    },
    headerText: { // Estilos para o texto do cabeçalho
        top:60,
        left:45,
        fontSize: 30, // Tamanho da fonte
        fontWeight: 'bold', // Estilo da fonte (negrito)
    },
    
});

export default Fantasias; // Exporta o componente Fantasias
